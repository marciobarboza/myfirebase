//
//  LoggedModel.swift
//  MyFirebase
//
//  Created by WEB 03 - SISTEMAS on 9/2/16.
//  Copyright © 2016 Barboza. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

protocol LoggedDelegate {
    func loggedDelegateSuccess()
    func loggedDelegateSuccessSaveBeer()
}

class LoggedModel: NSObject {
    var delegate:LoggedDelegate?
    
    func addBeer(name:String,content:Int) {
        let ref = FIRDatabase.database().reference()
        let userID = FIRAuth.auth()?.currentUser?.uid
        
        let key = ref.child("beers").childByAutoId().key
        
        let beer = ["name": name, "content": content, "uid": userID!]
        
        let childUpdates = ["/beers/\(key)": beer]
        
        ref.updateChildValues(childUpdates)
        
        delegate?.loggedDelegateSuccessSaveBeer()
    }
    
    func logout() {
        try! FIRAuth.auth()!.signOut()
        delegate?.loggedDelegateSuccess()
    }
}
