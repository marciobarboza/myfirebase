//
//  LoginModel.swift
//  MyFirebase
//
//  Created by WEB 03 - SISTEMAS on 9/2/16.
//  Copyright © 2016 Barboza. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol LoginModelDelegate {
    func loginModelDelegateSuccess()
    func loginModelDelegateFailed(message:String)
}
class LoginModel: NSObject {
    var delegate:LoginModelDelegate?
    
    override init() {
        super.init()
    }
    func login(email:String, password:String) {
        FIRAuth.auth()?.signInWithEmail(email, password: password) { (user, error) in
            if (user != nil) {
                print(user)
                self.delegate?.loginModelDelegateSuccess()
            } else {
                self.delegate?.loginModelDelegateFailed(NSLocalizedString("LOGIN_INVALID", comment: ""))
            }
        }
    }
}
