//
//  LoginViewController.swift
//  MyFirebase
//
//  Created by WEB 03 - SISTEMAS on 9/2/16.
//  Copyright © 2016 Barboza. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginModelDelegate {

    let model = LoginModel()
    
    @IBOutlet weak var textFieldEmail:UITextField!
    @IBOutlet weak var textFieldPassword:UITextField!
    @IBOutlet weak var buttonSignUp:UIButton!
    
    @IBAction func signIn() {
        model.login(textFieldEmail.text!, password: textFieldPassword.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        model.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginModelDelegateSuccess() {
        print("Sucesso!!")
        performSegueWithIdentifier("loginToLogged", sender: nil)
    }
    
    func loginModelDelegateFailed(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("APP_NAME", comment: ""),
                                      message: message,
                                      preferredStyle: .Alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .Cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
