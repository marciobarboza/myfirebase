//
//  LoggedViewController.swift
//  MyFirebase
//
//  Created by WEB 03 - SISTEMAS on 9/2/16.
//  Copyright © 2016 Barboza. All rights reserved.
//

import UIKit

class LoggedViewController: UIViewController, LoggedDelegate {
    
    var loggedModel = LoggedModel()
    @IBOutlet weak var textFiledName:UITextField!
    @IBOutlet weak var textFiledContent:UITextField!
    
    @IBAction func logout() {
        loggedModel.logout()
    }
    
    @IBAction func addBeer() {
        loggedModel.addBeer(textFiledName.text!, content: Int(textFiledContent.text!)!)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        loggedModel.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loggedDelegateSuccessSaveBeer() {
        print("Gravado")
        textFiledName.text = "";
        textFiledContent.text = "";
    }
    func loggedDelegateSuccess() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
